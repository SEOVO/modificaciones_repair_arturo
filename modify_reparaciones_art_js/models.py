from odoo import fields, models , api

class RepairLine(models.Model):
    _inherit = 'repair.line'
    price_tax = fields.Float(compute='_compute_amount_js', string='Total Tax', readonly=True, store=True)
    price_total = fields.Float(compute='_compute_amount_js', string='Total', readonly=True, store=True)

    @api.depends('product_uom_qty', 'price_unit', 'tax_id')
    def _compute_amount_js(self):
        """
        Compute the amounts of the SO line.
        """
        for line in self:
            price = line.price_unit
            taxes = line.tax_id.compute_all(price, line.repair_id.pricelist_id.currency_id, line.product_uom_qty,
                                            product=line.product_id, partner=line.repair_id.partner_id)
            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                #'price_subtotal': taxes['total_excluded'],
            })

class RepairFee(models.Model):
    _inherit = 'repair.fee'
    price_tax = fields.Float(compute='_compute_amount_js', string='Total Tax', readonly=True, store=True)
    price_total = fields.Float(compute='_compute_amount_js', string='Total', readonly=True, store=True)

    @api.depends('product_uom_qty', 'price_unit', 'tax_id')
    def _compute_amount_js(self):
        """
        Compute the amounts of the SO line.
        """
        for line in self:
            price = line.price_unit
            taxes = line.tax_id.compute_all(price, line.repair_id.pricelist_id.currency_id, line.product_uom_qty,
                                            product=line.product_id, partner=line.repair_id.partner_id)
            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                #'price_subtotal': taxes['total_excluded'],
            })